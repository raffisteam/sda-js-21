(function () {

    if (true) {
        console.log("do it...");
    } else {
        console.log("do something else")
    }
    // shortcut for simple if else
    console.log(true ? "do it" : "do something else");


    var zmiennaTekstowa = "przyklad tekstowej zmiennej";
    var zmiennaBoolean = true;
    if (typeof zmiennaTekstowa === 'string') {
        console.log("Zmienna jest tekstowa");
    }

    if (typeof zmiennaBoolean !== 'boolean') {
    } else {
        console.log("zmienna jest typu boolean");
    }


    console.log("Podwojny znak ==");
    console.log("1" == 1 ? "TAK" : "NIE");

    console.log("Potrojny znak ===");
    console.log("1" === 1 ? "TAK" : "NIE");

    console.log("Czy 'true' jest prawdziwe?");
    console.log("true" ? "TAK" : "NIE");
    console.log("Porownanie zmiennej tekstowej z boolean (==)");
    console.log("true" == true ? "TAK" : "NIE");
    console.log("Porownanie zmiennej tekstowej z boolean (===)");
    console.log("true" === true ? "TAK" : "NIE");

    // operator if
    let warunek1 = true, warunek2 = false, warunek3 = true;
    if ((warunek1 && warunek2) || warunek3) {
        // let do it...
    }

    const zmiennaNull = "";
    const orVar = zmiennaNull || "Hardcoded tekst";
    console.log(orVar);

    // przykład switcha

    const randomNumber = Math.floor(Math.random() * 4); // wylosowanie wartosci 0, 1, 2 lub 3
    console.log(randomNumber);
    switch (randomNumber) {
        case 0: {
            console.log("Tyle co nic");
            break;
        }
        case 1: {
            console.log("Słaba jedynka");
            break;
        }
        case 2: {
            console.log("Sliczna dwojka");
            break;
        }
        default: {
            console.log("Liczba wieksza niz 2");
        }
    }
})();